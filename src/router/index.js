import Vue from 'vue'
import VueRouter from 'vue-router'
// 导入子组件----------------------------------
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
// 导入home子组件
import Welcome from '../components/Welcome.vue'
// 导入 users 子组件
import Users from '../components/users/users.vue'
// 导入 righit 子组件
import Right from '../components/Power/Right.vue'
// 导入 Roles 子组件
import Roles from '../components/Power/Roles.vue'
// 导入商品分类子组件
import Cate from '../components/goods/Cate.vue'
// 导入分类参数组件
import Params from '../components/goods/Goods_params.vue'
// 引入商品列表
import Goods from '../components/goods/list.vue'
// 引入商品新增页面
import Add from '../components/goods/add.vue'
// 引入订单功能页面
import Order from '../components/order/Order.vue'

// 注册全局路由构造函数, 在其他 组件中也可以访问
Vue.use(VueRouter)

// 定义路由规则数组
const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/home',
    component: Home,
    redirect: '/Welcome',
    children: [
      // home 主页
      {
        path: '/Welcome',
        component: Welcome
      },
      // 用户操作路由
      {
        path: '/users',
        component: Users
      },
      // 用户权限路由
      {
        path: '/rights',
        component: Right
      },
      {
        path: '/roles',
        component: Roles
      },
      {
        path: '/categories',
        component: Cate
      },
      {
        path: '/params',
        component: Params
      },
      {
        path: '/goods',
        component: Goods
      },
      {
        path: '/goods/add',
        component: Add
      },
      {
        path: '/orders',
        component: Order
      }
    ]
  }
]

// 创建路由对象
const router = new VueRouter({
  routes
})

// 设置路由守卫函数
// 作用: 在浏览器地址栏 hash值 发生改变触发 , 可以用来检查 当前访问的hash路径 是否需要有token
router.beforeEach((to, form, next) => {
  // 判断是否 访问得 为login, 如果是 , 则放行
  if (to.path === '/login') return next()
  // 否则, 方式获取 内存中保存的token值
  const strToken = window.sessionStorage.getItem('token')
  // 如果不存在 token, 则强制 将hash值改成 /login
  if (!strToken) return next('/login')
  // 如果存在 token, 则放行
  next()
})

export default router
