import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入 阿里图标
import './assets/fonts/iconfont.css'
// 引入element-ui的注册文件
import './plugins/element.js'
import ElementUI, { Message } from 'element-ui'
import './assets/css/global.css'
// 引入tree-table样式包
import ZkTable from 'vue-table-with-tree-grid'
// 引入momen包
import moment from 'moment'
// 引入富文本组件
import VueQuillEditor from 'vue-quill-editor'
// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

// 1. axios相关 ------------------------------------------------
// 引入axios包
import axios from 'axios'
// 注册全局富文本
Vue.use(VueQuillEditor)

// 引入tree-table样式包 注册vue全局属性
Vue.component('tree-table', ZkTable)

Vue.filter('dateFormat', val => {
  return moment.unix(val).format('YYYY-MM-DD HH:mm:ss')
})
Vue.use(ElementUI)
Vue.prototype.$smg = Message
// 设置axios基地址
axios.defaults.baseURL = 'http://localhost:8888/api/private/v1/'
// 设置请求 拦截器: 添加Authorization到请求头
axios.interceptors.request.use((req) => {
  req.headers.Authorization = window.sessionStorage.getItem('token')
  // 返回修改后的 请求报文数据对象
  return req
})
// 挂载 axios到 Vue原型中
// 引入axios 并添加到原型中, 后面的所有的vue实例 和子类的实例, 都可以访问
Vue.prototype.$http = axios
Vue.prototype.$axios = axios
// 阻止启动产生消息
Vue.config.productionTip = false
// --------------------------------------------------------------
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
